package com.mii.poc.backend.kafka.consumer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mii.poc.backend.kafka.consumer.entity.Asken;

@Repository
public interface AskenRepository extends JpaRepository<Asken, Long>{
List<Asken> findById(String id);
List<Asken> findAll();
}