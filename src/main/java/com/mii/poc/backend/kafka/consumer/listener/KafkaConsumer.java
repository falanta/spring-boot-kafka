package com.mii.poc.backend.kafka.consumer.listener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mii.poc.backend.kafka.consumer.entity.Asjiw;
import com.mii.poc.backend.kafka.consumer.entity.Asken;
import com.mii.poc.backend.kafka.consumer.repository.AsjiwRepository;
import com.mii.poc.backend.kafka.consumer.repository.AskenRepository;


@Service
public class KafkaConsumer {
	 @Autowired
     AsjiwRepository repository;
	 
	 @Autowired
     AskenRepository askenrepository;
	
	
    @KafkaListener(topics = "users", groupId = "group_json",
            containerFactory = "asjiwKafkaListenerFactory")
    public void consumeJsonAsjiw(Asjiw asjiw) {
    	asjiw.getNama_lengkap();
        System.out.println("Consumed JSON Message: " + asjiw.getNama_lengkap());
        repository.save(asjiw);
        
        
    }
    
    @KafkaListener(topics = "asken", groupId = "group_json",
            containerFactory = "askenKafkaListenerFactory")
    public void consumeJsonAsken(Asken asken) {
    	asken.getNama_lengkap();
    
        System.out.println("Consumed JSON Message: " + asken.getNama_lengkap());
        askenrepository.save(asken);
        
        
    }
    
    
    
   
}