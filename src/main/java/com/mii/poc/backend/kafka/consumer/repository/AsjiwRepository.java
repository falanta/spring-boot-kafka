package com.mii.poc.backend.kafka.consumer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mii.poc.backend.kafka.consumer.entity.Asjiw;

@Repository
public interface AsjiwRepository extends JpaRepository<Asjiw, Long>{
List<Asjiw> findById(String id);
List<Asjiw> findAll();
}

